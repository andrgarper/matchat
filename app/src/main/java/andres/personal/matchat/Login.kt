package andres.personal.matchat

import andres.personal.matchat.models.User
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Login : AppCompatActivity() {
    //en el android manifest pone que esta es la vista principal
    //recibo las variables de los componentes de la vista
    private lateinit var email: EditText
    private lateinit var pas: EditText
    private lateinit var btSend: ImageButton
    private lateinit var btNew: ImageButton
    //como el proyecto esta hecho con firebase necesito logear con auth
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //quito la barra asquerosa porque no me gusta
        supportActionBar?.hide()
        //inicializo auth como acabas de entrar a la app no hay nada en auth
        auth = FirebaseAuth.getInstance()

        //recibo todos los componentes de la vista por id
        email = findViewById(R.id.logEmail)
        pas = findViewById(R.id.logPas)
        btSend = findViewById(R.id.logBtSend)
        btNew = findViewById(R.id.logBtNew)
        //pongo un listener en el click que basicamente hace la comprobacion de login
        btSend.setOnClickListener {
            //primero chequeo que no tenga un campo vacio
            if (email.text.toString().isNotEmpty() && pas.text.toString().isNotEmpty()) {
                login(email.text.toString(), pas.text.toString())
            } else {
                //mando un texto por pantalla
                //(todos los toast son textos que tienen: un contexto, el texto que muestra y un numero de milisegundos que sera mostrado)
                Toast.makeText(applicationContext, "Fiera rellena los campos!!", Toast.LENGTH_LONG)
                    .show()
            }
        }

        //hago un listner el cual manda a otra actividad
        btNew.setOnClickListener {
            //al querer cambiar de actividad tienes que usar este metodo y crear un itent necesitan un contexto y una clase java a la que irse
            startActivity(Intent(this, Register::class.java))
        }
    }
    //aqui esta lo importante de esta vista
    private fun login(correo: String, contraseña: String) {
        //ejecuta el entrar con correo y contraseña
        auth.signInWithEmailAndPassword(correo, contraseña)
            .addOnCompleteListener(this)
            //al ser asincrono le ponemos un listener al terminar
            { //llamamos a la task tarea porque somos mu guays
                    tarea ->
                //si el codigo anterior funciono
                if (tarea.isSuccessful) {
                    //y el correo esta verificado
                    if (chekear()) {

                        startActivity(Intent(this, MainActivity::class.java))
                        finish()

                    }
                } else {
                    //si esos datos no coinciden con ningun usuario muestra me este texto
                    Toast.makeText(
                        applicationContext,
                        "Los datos no coinciden!!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }



    private fun chekear(): Boolean {
        //creamos un bolean? que puede ser nulo
        var check: Boolean?;
        //como auth.currentUser puede ser nulo tines que poner un ?
        check = auth.currentUser?.isEmailVerified
        //para que me deje hacer una comprobacion tengo que chekear que no sea nulo colo no lo va a ser nunca
        if (check != null) {
            if (!check) {
                //si el correo no esta verificado envia esto
                Toast.makeText(
                    applicationContext,
                    "El email no esta verificado miralo!!",
                    Toast.LENGTH_LONG
                ).show()

            }
            return check
        }
        //aqui no deberia llegar pero dile tu al compilador que nunca sera nulo el current porque lo has controlado tu
        //y como no puedes hablar con el compilador te fastidias y puner un != null y un return false q nunca llegara
        return false

    }
}