package andres.personal.matchat

import andres.personal.matchat.models.User
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    //recibo los objetos de la vista
    private lateinit var userRecyclerView: RecyclerView
    //la lista me sirve para crear el recycler view
    private lateinit var userList: ArrayList<User>
    //los malditos recyclerView necesitan un adapter
    private lateinit var adapter: UserAdapter
    //pos el auth
    private lateinit var auth: FirebaseAuth
    //la referencia para leer los datos
    private lateinit var ref: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //creo la lista vacia
        userList = ArrayList()
        //creo el adaptador que he hecho en otra clase
        adapter = UserAdapter(this, userList)

        auth = FirebaseAuth.getInstance()
        //obtengo el recicler
        userRecyclerView = findViewById(R.id.mRv)
        //obtengo la referencia
        ref = Firebase.database("https://kt-login-d3cda-default-rtdb.europe-west1.firebasedatabase.app/").getReference()
        //al recycler view le pongo un linearLayoutManager para que se alineen los objetos verticalmente
        userRecyclerView.layoutManager = LinearLayoutManager(this)
        //le doy el adaptador que necesita
        userRecyclerView.adapter = adapter
        //recibo los datos de la tabla usuarios
        ref.child("users").addValueEventListener(object : ValueEventListener {
            //si los datos de la base de datos cambian
            //la variable snapshot son los datos de la query
            override fun onDataChange(snapshot: DataSnapshot) {
                //limpio la lista que ya tenia
                userList.clear()
                //creo la variable que usare para leer cada usuario
                var currentUser: User?
                //le hago un for each y los meto en la lista siempre y cuando no sean tu mismo
                //ya que no tiene sentido que puedas mandarte mensajes a ti mismo
                for (postSnapshot in snapshot.children) {

                    currentUser = postSnapshot.getValue(User::class.java)

                    if (!currentUser?.uid.equals(auth.currentUser?.uid)) {
                        userList.add(currentUser!!)
                    }

                }
                //avisale al adaptador que los datos han cambiado para que se refresque
                adapter.notifyDataSetChanged()
            }
            //funcion obligatoria a este listener
            override fun onCancelled(error: DatabaseError) {

            }

        })
    }

    //hice un menu que son lo tipico de los 3 puntitos
    //necesita el metodo este para crearse
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
    //basicamente este es el metodo que realiza en el caso de que pulses el boton de logout
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId.equals(R.id.menuLogOut)) {
            auth.signOut()
            startActivity(Intent(this, Login::class.java))
            finish()
            return true
        }
        return false
    }
}