package andres.personal.matchat

import andres.personal.matchat.models.User
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class Register : AppCompatActivity() {
    //recibo los objetos de la vista
    private lateinit var back:ImageView
    private lateinit var bt:ImageButton
    private lateinit var email:EditText
    private lateinit var pas:EditText
    private lateinit var pas2:EditText
    private lateinit var nick:EditText
    //recibo lo relacionado con firebase
    private lateinit var auth:FirebaseAuth
    //esto me deja acceder a la referencia
    private lateinit var database:FirebaseDatabase
    //la referencia es al fin de cuentas lo que me da datos y me deja subirlos
    private lateinit var ref: DatabaseReference



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        //consigo la instancia de auth
        auth = FirebaseAuth.getInstance()
        //consigo la base de datos muy importante poner el enlace
        database = Firebase.database("https://kt-login-d3cda-default-rtdb.europe-west1.firebasedatabase.app/")
        //conseguimos la referencia
        ref = database.getReference()

        //odio la maldita barra
        supportActionBar?.hide()

        //cojo toda la vista
        back = findViewById(R.id.regBack)
        email = findViewById(R.id.regEmail)
        bt = findViewById(R.id.regBt)
        pas = findViewById(R.id.regPas)
        pas2 = findViewById(R.id.regPas2)
        nick = findViewById(R.id.regNick)
        //si pulsas aqui eliminas esta actividad y iria al main que es login
        back.setOnClickListener {
            finish()
        }

        bt.setOnClickListener {
            //para enseñarte como acceder al texto de un edit text creo estas variables


             email.text.toString()
             pas.text.toString()
             pas2.text.toString()

            //compruebo que las contraseñas sean iguales y que los campos no esten vacios y que la contraseña sea 6 o mas

            if (sonIguales(pas.text.toString(),pas2.text.toString()) && estanLLenos(pas.text.toString(), email.text.toString(),nick.text.toString())&&tamanyoMinimo(pas.text.toString()))
            {
                registrar(email.text.toString(),pas.text.toString())
            }
        }

    }

    private fun registrar(textCorreo: String, textContraseña: String) {
        //hay diferentes tipos de log pero los log.D solo se ven debug
        Log.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!","el correo es: "+textCorreo+" la contraseña es: "+textContraseña)
        //este metodo te crea un usuario con email y contraseña
        auth.createUserWithEmailAndPassword(textCorreo,textContraseña).addOnCompleteListener(this)
        { //la llamo tarea porque quiero y puedo la puedes llamar pepe si quieres
                tarea ->


            if(tarea.isSuccessful){
                //cojo el usuario que cree y le mando un email de verificacion si lo verifica le mando un texto diciendole que lo hizo bien y lo mando pal login
                Toast.makeText(applicationContext, "Revisa tu correo, verficalo!!", Toast.LENGTH_LONG ).show()
                auth.currentUser?.sendEmailVerification()?.addOnCompleteListener(this){ tarea ->
                    if(tarea.isSuccessful) {
                        // el !! hace que tire para alante independiente mente de si es nula o no es decir bienvenido a null pointer exeption
                        addUserToDatabase(textCorreo, textContraseña,auth.currentUser?.uid!!,nick.text.toString())
                        finish()
                    }

                }

            }
            //esto es como un instance of de toda la vida pero si no sabes lo que es te lo explico
            //dice algo como Si X pertenece a Clase haz esto (donde X es una variable y Clase cualquier clase)
            //esto dice si la exepcion de la tarea es una del tipo FirebaseAuthUserCollisionException muestra este mensaje
            else if (tarea.exception is FirebaseAuthUserCollisionException){
                //             la aplicacion            texto del error      3,5 segundos      mostrar

                Toast.makeText(applicationContext, "El correo ya existe!!", Toast.LENGTH_LONG ).show()
            }
            else if (tarea.exception is FirebaseAuthInvalidCredentialsException){

                Toast.makeText(applicationContext, "El correo esta mal escrito!!", Toast.LENGTH_LONG ).show()
            }
            else{
                //si llegas aqui no se que puede pasar la verdad no se tanto
                Toast.makeText(applicationContext, "Error desconocido, hackeaste el sistema?!!", Toast.LENGTH_LONG ).show()
            }
        }
    }

    private fun addUserToDatabase(correo: String, contraseña: String,uid:String,nick: String) {
        Log.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!","supuestamente deberia meter los datos")
        //en real time database tu haces child para crear o referirte a una tabla luego otra vez child para poner nombre al campo (id) y luego set values para poner los datos
        ref.child("users").child(uid).setValue(User(nick,correo,uid)).addOnCompleteListener {
            tarea ->
            if (tarea.isSuccessful){
                Log.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!","los deberia meter")
            }else{
                Log.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!","no los ha metido porque" + tarea.exception)
            }

        }
                //me estuvo dando problemas porque no puse el enlace en la base de datos
                //este listener es que ha habido un fallo que te muestre la exepcion
            .addOnFailureListener {
                e->
                Log.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!","no los ha metido 2 porque " + e )
            }
    }

    private fun estanLLenos(textContraseña: String, textCorreo: String,nick:String): Boolean {
        //si la contraseña esta vacia o el correo esta vacio muestra este mensaje
        if(textContraseña.isEmpty()||textCorreo.isEmpty()||nick.isEmpty()){
            Toast.makeText(applicationContext, "La contraseña o el correo estan vacios o el nick!!", Toast.LENGTH_LONG ).show()
        }

        return textContraseña.isNotEmpty()&&textCorreo.isNotEmpty()
    }

    private fun sonIguales(textContraseña: String, textRepiteContraseña: String): Boolean {
        //si la contraseña y la otra no son iguales enseña este mensaje
        if (!textContraseña.equals(textRepiteContraseña)){
            Toast.makeText(applicationContext, "Las contraseñas no coinciden!!", Toast.LENGTH_LONG ).show()
        }
        return textContraseña.equals(textRepiteContraseña);
    }

    private fun tamanyoMinimo(textContraseña: String): Boolean {
        //si el tamaño de la contaseña es menor que 6 enseña este texto
        if (textContraseña.length<6){
            Toast.makeText(applicationContext, "tu contraseña no llega ni a 6 caracteres pon 6 como minimo!!", Toast.LENGTH_LONG ).show()
        }
        return textContraseña.length>=6;
    }

}